import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from "src/app/app.component";

const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    {
        path: 'inicio/listaPessoa',
        loadChildren: './modules/inicio/inicio.module#InicioModule',
    }
    
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]

})
export class AppRoutingModule {

}