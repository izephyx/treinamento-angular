import { Component } from "@angular/core";
import { InicioService } from "src/app/modules/inicio/inicio.service";

@Component({
    selector: 'app-inicio',
    templateUrl: './inicio.component.html',
    styleUrls: ['./inicio.component.css']
})
export class InicioComponent {
    private pessoas = [];

    constructor(private inicioService: InicioService) { }


    ngOnInit() {
        this.inicioService.getPessoa().subscribe(resp => {
            console.log(resp);
            
            this.pessoas = resp;
        });
        
        
    }
}