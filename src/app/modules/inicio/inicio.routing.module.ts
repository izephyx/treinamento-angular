import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from "src/app/modules/inicio/inicio/inicio.component";

const routes: Routes = [
    {
        path: '',
        component: InicioComponent
    }
];

@NgModule({
    imports:[
        RouterModule.forChild(routes)
    ],
    exports:[
        RouterModule
    ]

})
export class InicioRoutingModule {

}