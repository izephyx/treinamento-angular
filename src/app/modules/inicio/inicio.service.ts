import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class InicioService {
    constructor(private http: HttpClient) { }


    getPessoa(): Observable<any[]> {
        return this.http.get<any[]>('http://localhost:9000/api/pessoas');
    }
}