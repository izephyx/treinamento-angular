import { NgModule } from "@angular/core";
import { InicioRoutingModule } from "src/app/modules/inicio/inicio.routing.module";
import { InicioService } from "src/app/modules/inicio/inicio.service";
import { SharedModule } from "src/app/shared/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { InicioComponent } from "src/app/modules/inicio/inicio/inicio.component";

@NgModule({
    imports:[
        InicioRoutingModule,
        SharedModule
    ],
    exports:[

    ],
    declarations:[
        InicioComponent
    ],
    providers:[
        InicioService
    ]
})
export class InicioModule{

}