import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { MatTableModule, MatInputModule, MAT_DIALOG_DATA, MatToolbarModule, MatMenuModule, MatSidenavModule, MatIconModule, MatListModule, MatProgressSpinnerModule, MatSortModule, MatDatepickerModule, MatSelectModule, MatNativeDateModule } from '@angular/material';
import { MatDialogRef, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        MatTableModule,
        MatInputModule,
        MatDialogModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatSortModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ],
    exports: [
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        MatTableModule,
        MatInputModule,
        MatDialogModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatSortModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ],
})
export class MaterialModule {

}