import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { MaterialModule } from "src/app/shared/material/material.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
    imports:[
        CommonModule,
        MaterialModule,
        ReactiveFormsModule
    ],
    exports:[
        CommonModule
    ],
    declarations:[

    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})
export class SharedModule {

}